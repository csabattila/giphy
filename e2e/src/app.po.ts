import { browser, by, element, ElementArrayFinder, ElementFinder, protractor } from 'protractor';

export class AppPage {
  static isVisibleAfterWait(el: ElementFinder, timeout = 5000): Promise<boolean> {
    return browser.wait(protractor.ExpectedConditions.visibilityOf(el), timeout)
      .then(() => true)
      .catch(() => false) as Promise<boolean>;
  }

  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('app-search mat-label')).getText() as Promise<string>;
  }

  getForm(): ElementFinder {
    return element(by.css('app-search app-search-form'));
  }

  getResultDiv(): ElementFinder {
    return element(by.css('app-search > div'));
  }

  getLoader(): ElementFinder {
    return element(by.css('app-search > .loader'));
  }

  getFormInput(): ElementFinder {
    return element(by.css('[formcontrolname="q"]'));
  }

  getFormSubmit(): ElementFinder {
    return element(by.css('app-search app-search-form button'));
  }

  getFormError(): ElementFinder {
    return element(by.css('app-search-form mat-error'));
  }

  getFormErrorText(): Promise<string> {
    return element(by.css('app-search-form mat-error p')).getText() as Promise<string>;
  }

  getPaginator(): ElementFinder {
    return element(by.css('app-search-paginator'));
  }

  getResultList(): ElementFinder {
    return element(by.css('mat-list'));
  }

  getResultItems(): ElementArrayFinder {
    return element.all(by.css('mat-list mat-list-item'));
  }
}
