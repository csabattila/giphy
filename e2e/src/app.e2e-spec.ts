import { browser, logging, protractor } from 'protractor';
import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeAll (() => {
    page = new AppPage();
    page.navigateTo();
  });

  it('should display form', () => {
    expect(page.getForm()).toBeDefined();
  });

  it('should display what the form is about', () => {
    expect(page.getTitleText()).toEqual('Search giphy');
  });

  it('should display form button', () => {
    expect(page.getFormSubmit()).toBeDefined();
  });

  it('should not run query if the form field is not valid', async () => {
    const input = await page.getFormInput();
    const submit = await page.getFormSubmit();

    // deleting love (kinda ugly) still better than sending 4 back_space
    const domInput = await input.getWebElement();
    await browser.executeScript('arguments[0].select();', domInput);

    await input.sendKeys(protractor.Key.BACK_SPACE);

    await submit.click();
    expect(await AppPage.isVisibleAfterWait(page.getFormError())).toBe(true);
    // expect(await AppPage.isVisibleAfterWait(page.getLoader())).not.toBe(true);
    expect(await page.getFormErrorText()).toEqual('Please, enter a query');

    await input.sendKeys('c');
    expect(AppPage.isVisibleAfterWait(page.getFormError())).not.toBe(true);

    await input.sendKeys('unt love');
    await submit.click();
    expect(AppPage.isVisibleAfterWait(page.getFormError())).toBe(true);
    expect(AppPage.isVisibleAfterWait(page.getResultList())).not.toBe(true);
    expect(await page.getFormErrorText()).toEqual('Please, do NOT enter profanity in you query. Delete the following profane words: cunt');
  });

  it('should run query if the form field is valid', async () => {
    const input = await page.getFormInput();
    const submit = await page.getFormSubmit();

    const domInput = await input.getWebElement();
    await browser.executeScript('arguments[0].select();', domInput);

    await input.sendKeys(protractor.Key.BACK_SPACE);

    input.sendKeys('love');
    await submit.click();

    expect(await AppPage.isVisibleAfterWait(page.getResultList())).toBe(true);
    expect(await page.getResultItems().count()).toBe(25);
    expect(await AppPage.isVisibleAfterWait(page.getPaginator())).toBe(true);

    expect(await AppPage.isVisibleAfterWait(page.getFormError())).toBe(false);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
