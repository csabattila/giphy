# Giphy

## About

Query Giphy Search API for gif images.

Initially will fetch max 25 images. You can change that, if possible, via the pagination.

The app uses a very basic cache in order to store the already fetched data for a certain offset and limit.

If the limit or the query changes it will invalidate it. 

Further improvement would be to make it a bit more clever so it keeps the cache data per offset, limit and query. 

However it is not clear the need of such a complex caching as if someone uploads a new image we will not get it until we invalidate the cache.

The form filters bad words via a third party and won't let the user to send queries with profanity. 

The styling is based on material with a quick custom theme.

## Install

Run `npm i` in the root folder of the projet.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/search`. The app will automatically reload if you change any of the source files.

## Code scaffolding

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-- --prod` flag for a production build.

## Running unit tests

Run `npm test test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Note 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.
