import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, last, map, switchMap, take, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Giphy, SearchQuery } from '../types';

@Injectable()
export class SearchService {
  static readonly DEFAULT_LIMIT: string = '10';

  public result$: BehaviorSubject<Giphy | null> = new BehaviorSubject<Giphy | null>(null);
  public updateResultsByQuery$: Subject<string> = new Subject<string>();
  public updateResultsByOffset$: BehaviorSubject<string> = new BehaviorSubject<string>('0');
  public updateResultsByLimit$: BehaviorSubject<string> = new BehaviorSubject<string>(SearchService.DEFAULT_LIMIT);

  static extract({data, pagination}: Giphy): Giphy {
    return {
      data: data.map((dataItem: any) => ({
        width: dataItem?.images?.fixed_height?.width,
        url: dataItem?.images?.fixed_height?.url,
        title: dataItem?.title
      })),
      ...{pagination}
    };
  }

  backwardLimitCache: Giphy;

  constructor(private http: HttpClient) {
    combineLatest([
      this.updateResultsByQuery$.pipe(
        distinctUntilChanged()
      ),
      this.updateResultsByOffset$
        .pipe(
          debounceTime(200),
          distinctUntilChanged(),
        ),
      this.updateResultsByLimit$.pipe(
        distinctUntilChanged()
      )],
    )
      .pipe(
        map((searchData: Array<string>): SearchQuery => ({
          q: searchData[0],
          offset: searchData[1],
          limit: searchData[2]
        }) as SearchQuery),
        tap((searchQuery: SearchQuery) => {
          const currentCount = Number(searchQuery.limit);
          const currentOffset = Number(searchQuery.offset);

          if (!!this.backwardLimitCache &&
            this.backwardLimitCache.pagination.count > currentCount &&
            this.backwardLimitCache.pagination.offset === currentOffset
          ) {
            this.result$.next({
              data: this.backwardLimitCache.data.slice(0, currentCount),
              pagination: {
                ...this.backwardLimitCache.pagination,
                count: currentCount
              }
            });
          }
        }),
        distinctUntilChanged((prevQuery: SearchQuery, currQuery: SearchQuery) =>
          prevQuery.offset === currQuery.offset && prevQuery.offset !== '0'
        ),
        map((searchQuery: SearchQuery): HttpParams =>
          new HttpParams({fromObject: {...searchQuery}}),
        ),
        switchMap((params: HttpParams) => (this.http.get(environment.url, {params}))),
        tap(console.warn),
        map(SearchService.extract),
        tap(({data, pagination}) => this.backwardLimitCache = {data, pagination})
      ).subscribe(({data, pagination}: Giphy) => {
        this.result$.next({data, pagination});
      }
    );
  }
}
