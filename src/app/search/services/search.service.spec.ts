import { HttpParams } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { SearchQuery } from '../types';
import { SearchService } from './search.service';

const mock1 = JSON.parse(`{
  "data": [{
    "title": "nbc b99 GIF by Brooklyn Nine-Nine",
    "images": {
      "fixed_height": {
        "width": "356",
        "url": "https://media1.giphy.com/media/xTiQyMeyAzJg9DAReo/200.gif"
      }
    }
  }, {
    "title": "disney set GIF",
    "images": {
      "fixed_height": {
        "width": "200",
        "url": "https://media1.giphy.com/media/woxbcftN4LzcA/200.gif"
      }
    }
  }, {
    "title": "britney spears GIF",
    "images": {
      "fixed_height": {
        "width": "200",
        "url": "https://media0.giphy.com/media/8p05WdXxPiOyY/200.gif"
      }
    }
  }],
  "pagination": {
    "total_count": 6,
    "count": 3,
    "offset": 0
  }
}`);

const mock2 = JSON.parse(`{
  "data": [{
    "title": "nbc b99 GIF by Brooklyn Nine-Nine 1",
    "images": {
      "fixed_height": {
        "width": "357",
        "url": "https://media1.giphy.com/media/xTiQyMeyAzJg9DAReo/200.gif"
      }
    }
  }, {
    "title": "disney set GIF 1",
    "images": {
      "fixed_height": {
        "width": "201",
        "url": "https://media1.giphy.com/media/woxbcftN4LzcA/200.gif"
      }
    }
  }, {
    "title": "britney spears GIF 1",
    "images": {
      "fixed_height": {
        "width": "201",
        "url": "https://media0.giphy.com/media/8p05WdXxPiOyY/200.gif"
      }
    }
  }],
  "pagination": {"total_count": 6, "count": 3, "offset": 3}
}`);

// todo rewrite
xdescribe('Service: SearchService', () => {
  let injector: TestBed;
  let search: SearchService;
  let searchPrivate: any;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SearchService],
    });

    injector = getTestBed();
    search = injector.inject(SearchService);
    searchPrivate = search;
    httpMock = injector.inject(HttpTestingController);

    // adjust default search options
    searchPrivate.searchQuery = {
      offset: '0',
      limit: '3',
      q: ''
    };
  });

  afterEach(() => {
    httpMock.verify();
  });

  const getQString = (): string => {
    return new HttpParams({fromObject: {...searchPrivate.searchQuery}}).toString();
  };

  const setResultExpectation = () => {
    search.result$.pipe(
      filter(Boolean))
      .subscribe((res) => {
        expect(res).toEqual({
          data: CacheService.getCacheEntry(searchPrivate.searchQuery.offset), // reference also checking if it is cached
          pagination: (search as any).currentPagination // checking if currentPagination is updated
        });
      });
  };

  it('should call http when no cache entry for requested offset is present', () => {
    setResultExpectation();

    const params: Pick<SearchQuery, 'q'> =  {q: 'love'};

    search.getGiphies(params);

    expect(searchPrivate.searchQuery).toEqual({q: 'love', offset: '0', limit: '3'});

    const qParamsString = getQString();
    const req = httpMock.expectOne(`${ environment.url }?${ qParamsString }`);

    expect(req.request.method).toBe('GET');
    req.flush(mock1);
  });

  // bit too big but also testing the sequences
  it('should handle cache accordingly', () => {
    setResultExpectation();

    // -> first call
    let params: Pick<SearchQuery, 'q'> | Omit<SearchQuery, 'q'> =  { q: 'love' };

    search.getGiphies(params);

    let qParamsString = getQString();
    let req = httpMock.expectOne(`${ environment.url }?${ qParamsString }`);

    req.flush(mock1);

    // -> pagination
    params =  { offset: '3', limit: '3'};

    search.getGiphies(params);

    qParamsString = getQString();
    req = httpMock.expectOne(`${ environment.url }?${ qParamsString }`);

    req.flush(mock2);

    // -> cache back 1
    params =  {offset: '0', limit: '3'};

    search.getGiphies(params);

    qParamsString = getQString();
    httpMock.expectNone(`${ environment.url }?${ qParamsString }`);

    // -> cache forward 1
    params =  {offset: '3', limit: '3'};

    search.getGiphies(params);

    qParamsString = getQString();
    httpMock.expectNone(`${ environment.url }?${ qParamsString }`);

    // -> cache fetch love again
    params = { q: 'love' };

    search.getGiphies(params);

    qParamsString = getQString();
    httpMock.expectNone(`${ environment.url }?${ qParamsString }`);

    // we were offset 3 but now it is back to 0
    expect(searchPrivate.currentPagination.offset).toEqual(0);

    // invalidate cache
    spyOn(CacheService, 'invalidateCache').and.callThrough();

    params =  {offset: '0', limit: '6'};

    search.getGiphies(params);

    qParamsString = getQString();

    req = httpMock.expectOne(`${ environment.url }?${ qParamsString }`);
    req.flush(mock1);

    expect(CacheService.invalidateCache).toHaveBeenCalled();
  });
});
