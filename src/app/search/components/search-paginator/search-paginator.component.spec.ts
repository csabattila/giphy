import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SearchService } from '../../services/search.service';
import { SearchPaginatorComponent } from './search-paginator.component';

describe('SearchPaginatorComponent', () => {
  let component: SearchPaginatorComponent;
  let fixture: ComponentFixture<SearchPaginatorComponent>;
  let search: jasmine.SpyObj<SearchService>;

  beforeEach(async(() => {
    search = jasmine.createSpyObj('SearchService', ['getGiphies']);
    search.getGiphies.and.returnValue();

    TestBed.configureTestingModule({
      declarations: [ SearchPaginatorComponent ],
      imports: [
        MatPaginatorModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: SearchService,
          useValue: search
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onPage', () => {
    it('should emit the search event went PageEvents happen', () => {
      component.onPage({pageSize: 50, pageIndex: 2, length: 100});

      expect(search.getGiphies).toHaveBeenCalledWith({limit: '50', offset: '100'});
    });
  });
});
