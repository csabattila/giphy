import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'app-search-paginator',
  templateUrl: './search-paginator.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPaginatorComponent {
  @Input() pagination: any;

  constructor(private searchService: SearchService) {
  }

  onPage($event: PageEvent): void {
    this.searchService.updateResultsByOffset$.next(($event.pageIndex * $event.pageSize).toString());
    this.searchService.updateResultsByLimit$.next($event.pageSize.toString());
  }
}
