import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-search-error',
  templateUrl: './search-error.component.html',
  styleUrls: ['./search-error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchErrorComponent {
  @Input() message: string;

  constructor() { }
}
