import { AbstractControl } from '@angular/forms';
import BadWordsFilter from 'bad-words';

export const badWordValidator = (control: AbstractControl): { [key: string]: any } | null => {
  const filter: BadWordsFilter = new BadWordsFilter();

  if (filter.isProfane(control.value)) {
    const badWords = control.value // will not collect those cases when two separate word are ok but together they are bad
      .trim()
      .replace(/\s/g, ' ')
      .split(' ')
      .reduce((agg: string[], item: string) => {
        if (filter.clean(item).replace(/\*/g, '') === '') {
          agg.push(item);
        }

        return agg;
      }, []).join(' ');

    return {'bad-words': badWords};
  } else {
    return null;
  }
};

