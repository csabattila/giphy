import { FormControl } from '@angular/forms';
import { removeSpacesOnlyValidator } from './remove-spaces-only.validator';

describe('removeSpacesOnlyValidator', () => {
  let control: FormControl;

  beforeEach(() => {
    control = new FormControl();
  });

  it('should trim the control value', () => {
    control.setValue(' ');
    const returnValue = removeSpacesOnlyValidator(control);

    expect(control.value).toBe('');
    expect(returnValue).toBeNull();

    control.setValue('   ');
    removeSpacesOnlyValidator(control);
    expect(control.value).toBe('');

    control.setValue('   a ');
    removeSpacesOnlyValidator(control);
    expect(control.value).toBe('   a ');
  });
});
