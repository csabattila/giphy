import { AbstractControl } from '@angular/forms';

// tricky but effective
export const removeSpacesOnlyValidator = (control: AbstractControl): null =>  {
  if (control && control.value && !control.value.replace(/\s/g, '').length) {
    control.setValue('');
  }
  return null;
};
