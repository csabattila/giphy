import { FormControl } from '@angular/forms';
import { badWordValidator } from './bad-word.validator';

describe('bad words validator', () => {
  let control: FormControl;

  beforeEach(() => {
    control = new FormControl();
  });

  it('should trim the control value', () => {
    control.setValue('cunt');
    expect(badWordValidator(control)).toEqual({'bad-words': 'cunt'});

    control.setValue('Mother Fucker hello love cunt');
    expect(badWordValidator(control)).toEqual({'bad-words': 'Fucker cunt'});
  });
});
