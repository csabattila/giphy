import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SearchService } from '../../services/search.service';
import { SearchFormComponent } from './search-form.component';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;
  let search: jasmine.SpyObj<SearchService>;

  beforeEach(async(() => {
    search = jasmine.createSpyObj('SearchService', ['getGiphies']);

    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        MatIconModule,
      ],
      declarations: [ SearchFormComponent ],
      providers: [
        {
          provide: SearchService,
          useValue: search,
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.form).toBeInstanceOf(FormGroup);
    expect(component.form.get('q')).toBeInstanceOf(FormControl);
  });

  describe('doSearch', () => {
    it('should not call service if form is invalid', () => {
      component.form.get('q')?.setValue('');
      expect(component.form.valid).toBeFalse();

      component.doSearch();
      expect(search.getGiphies).not.toHaveBeenCalled();
    });

    it('should call service if form is valid', () => {
      spyOn(component.search, 'emit');

      component.form.get('q')?.setValue('love');
      component.doSearch();

      expect(search.getGiphies).toHaveBeenCalledWith({q: 'love'});
      expect(component.search.emit).toHaveBeenCalled();
    });
  });
});
