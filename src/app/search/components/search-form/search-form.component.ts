import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SearchService } from '../../services/search.service';
import { badWordValidator } from './validators/bad-word.validator';
import { removeSpacesOnlyValidator } from './validators/remove-spaces-only.validator';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['search-form.component.scss'],
})
export class SearchFormComponent {
  form: FormGroup;

  constructor(
    private searchService: SearchService,
    fb: FormBuilder
  ) {
    this.form = fb.group(
      {
        q: fb.control('love', [Validators.required, badWordValidator, removeSpacesOnlyValidator])
      }
    );
  }

  doSearch(): void {
    if (this.form.valid) {
      const q = this.form.controls.q.value.trim();

      // always start with default offset
      this.searchService.updateResultsByOffset$.next('0');
      this.searchService.updateResultsByQuery$.next(q);
    }
  }
}
