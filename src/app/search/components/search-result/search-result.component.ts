import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { GiphyImage } from '../../types';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['search-result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultComponent {
  @Input() giphies: GiphyImage[];

  constructor() {
  }
}
