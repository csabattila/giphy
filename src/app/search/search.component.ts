import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchService } from './services/search.service';
import { Giphy } from './types/giphy';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  loading = false;

  result$: Observable<Giphy | null>;

  constructor(searchService: SearchService) {
    this.result$ = searchService.result$;
  }

  onSearch(): void {
    this.loading = true;
  }
}
