import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SearchFormComponent } from './components';
import { SearchResultComponent } from './components';
import { SearchPaginatorComponent } from './components';
import { SearchErrorComponent } from './components/search-error/search-error.component';
import { HttpApiKeyInterceptor } from './interceptors/api-key.interceptor';
import { HttpErrorInterceptor } from './interceptors/error.interceptor';
import { SearchComponent } from './search.component';
import { SearchService } from './services/search.service';

const MATERIALS = [
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  ReactiveFormsModule,
  HttpClientModule,
  MatPaginatorModule,
  MatListModule,
  MatProgressSpinnerModule,
];

@NgModule({
  imports: [
    CommonModule,
    ...MATERIALS
  ],
  declarations: [
    SearchFormComponent,
    SearchResultComponent,
    SearchComponent,
    SearchPaginatorComponent,
    SearchErrorComponent,
  ],
  exports: [
    SearchComponent,
  ],
  providers: [
    SearchService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpApiKeyInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
  ]
})
export class SearchModule {

}
