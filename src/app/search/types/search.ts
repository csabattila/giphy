export interface SearchQuery {
  q: string;
  limit: string;
  offset: string;
}
