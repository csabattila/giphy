export interface GiphyImage {
  url: string;
  title: string;
  width: number;
}

export interface GiphyPagination {
  total_count: number;
  count: number;
  offset: number;
}

export interface Giphy {
  data: Array<GiphyImage> | any;
  pagination: GiphyPagination;
  error?: {
    message: string
  };
}
